# frozen_string_literal: true

module Maintenance
  class SeedTask < MaintenanceTasks::Task
    def collection
      [Rails.root.join('db/seeds.rb')]
    end

    def process(file_path)
      load file_path
    end

    def count
      1
    end
  end
end
